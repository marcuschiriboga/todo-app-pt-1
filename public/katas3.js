const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
function katasNum(arr, answer) {
  let newDiv = document.createElement("div");
  newDiv.innerHTML = "<h2>Kata " + arr + " </h2>" + answer;
  document.body.appendChild(newDiv);
}
function kata1() {
  let answer = "";
  for (let i = 1; i <= 25; i++) {
    answer += i + ", ";
  }
  return answer;
}
katasNum(1, kata1());
function kata2() {
  let answer = "";
  for (let i = 25; i >= 1; i--) {
    answer += i + ", ";
  }
  return answer;
}
katasNum(2, kata2());
function kata3() {
  let answer = "";
  for (let i = -1; i >= -25; i--) {
    answer += i + ", ";
  }
  return answer;
}
katasNum(3, kata3());
function kata4() {
  let answer = "";
  for (let i = -25; i <= -1; i++) {
    answer += i + ", ";
  }
  return answer;
}
katasNum(4, kata4());
function kata5() {
  let answer = "";
  for (let i = 25; i >= -25; i--) {
    answer += i + ", ";
  }
  return answer;
}
katasNum(5, kata5());
function kata6() {
  let answer = "";
  for (let i = 3; i <= 100; i++) {
    if (i % 3 === 0) {
      answer += i + ", ";
    }
  }
  return answer;
}
katasNum(6, kata6());
function kata7() {
  let answer = "";
  for (let i = 7; i <= 100; i++) {
    if (i % 7 === 0) {
      answer += i + ", ";
    }
  }
  return answer;
}
katasNum(7, kata7());
function kata8() {
  let answer = "";
  for (let i = 100; i >= 3; i--) {
    if (i % 3 == 0) {
      answer += i + ", ";
    } else if (i % 7 == 0) {
      answer += i + ", ";
    }
  }
  return answer;
}
katasNum(8, kata8());
function kata9() {
  let answer = "";
  for (let i = 5; i <= 95; i++) {
    if (i % 5 === 0) {
      answer += i + ", ";
    }
  }
  return answer;
}
katasNum(9, kata9());
function kata10() {
  let answer = "";
  for (let i = 0; i < sampleArray.length; i++) {
    answer += sampleArray[i] + ", ";
  }
  return answer;
}
katasNum(10, kata10());
function kata11() {
  let answer = "";
  for (let i = 1; i < sampleArray.length; i++) {
    if (sampleArray[i] % 2 === 0) {
      answer += sampleArray[i] + ", ";
    }
  }
  return answer;
}
katasNum(11, kata11());
function kata12() {
  let answer = "";
  for (let i = 0; i < sampleArray.length; i++) {
    if (sampleArray[i] % 1 === 0) {
      answer += sampleArray[i] + ", ";
    }
  }
  return answer;
}
katasNum(12, kata12());
function kata13() {
  let answer = "";
  for (let i = 0; i < sampleArray.length; i++) {
    if (sampleArray[i] % 8 === 0) {
      answer += sampleArray[i] + ", ";
    }
  }
  return answer;
}
katasNum(13, kata13());
function kata14() {
  let answer = "";
  for (let i = 0; i < sampleArray.length; i++) {
    answer += sampleArray[i] * sampleArray[i] + ", ";
  }
  return answer;
}
katasNum(14, kata14());
function kata15() {
  let answer = 0;
  for (let i = 1; i <= 20; i++) {
    answer += i;
  }
  return answer;
}
katasNum(15, kata15());
function kata16() {
  let answer = 0;
  for (let i = 0; i < sampleArray.length; i++) {
    answer += sampleArray[i];
  }
  return answer;
}
katasNum(16, kata16());
function kata17() {
  let answer = sampleArray[0];
  for (let i = 0; i < sampleArray.length; i++) {
    if (sampleArray[i] < answer) {
      answer = sampleArray[i];
    }
  }
  return answer;
}
katasNum(17, kata17());
function kata18() {
  let answer = sampleArray[0];
  for (let i = 0; i < sampleArray.length; i++) {
    if (sampleArray[i] > answer) {
      answer = sampleArray[i];
    }
  }
  return answer;
}
katasNum(18, kata18());
// Was in a study group that went over the beginning of the code.
