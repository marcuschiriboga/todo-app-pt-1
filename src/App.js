import React, { Component } from "react";
import todosList from "./todos.json";

// forms from https://reactjs.org/docs/forms.html
// bugs for cleaning returning empty todos.

//
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: todosList,
      value: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.destroyFunction = this.destroyFunction.bind(this);
  }

  handleChange(event) {
    event.persist();
    console.log(event.target.value);
    this.setState((state) => {
      return {
        ...state,
        value: event.target.value,
      };
    });
  }

  handleSubmit(event) {
    event.persist();
    event.preventDefault();
    const idKey = this.state.todos.length + 1;
    this.setState((state) => {
      return {
        ...state,
        todos: [
          ...state.todos,
          {
            userId: 1,
            id: idKey,
            title: event.target.children[0].value,
            completed: false,
          },
        ],
        value: "",
      };
    });
  }
  handleToggle = (event) => {
    event.persist();
    const idKey = event.currentTarget.id - 1;
    const toggleVar = this.state.todos;
    console.log(toggleVar[idKey]);
    if (toggleVar[idKey].completed) {
      toggleVar[idKey].completed = false;
    } else {
      toggleVar[idKey].completed = true;
    }
    this.setState((state) => {
      return {
        ...state,
        todos: toggleVar,
      };
      console.log(this.state);
    });
  };
  destroyFunction = (event) => {
    event.persist();
    event.currentTarget.parentElement.parentElement.style.display = "none";
    //  temp fix keeps state array the same but removes it from screen
    //  this way idKey works with array.length.
    //
  };
  clearCompleted = (event) => {
    event.persist();
    const clearReset = this.state.todos;
    console.log(clearReset);
    // var clearVar = null;
    var clearVar = document.getElementsByClassName("completed");
    console.log(clearVar[1]);
    if (clearVar[0]) {
      for (let i = 0; i < clearVar.length; i++) {
        clearVar[i].style.display = "none";
        // document.style.display = "none";
        //  temp fix keeps state array the same but removes it from screen
        //  this way idKey works with array.length.
      }
    }
  };
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <form onSubmit={this.handleSubmit}>
            <input
              className="new-todo"
              placeholder="What needs to be done?"
              value={this.state.value}
              onChange={this.handleChange}
              autoFocus
            />
          </form>
        </header>
        <TodoList todos={this.state.todos} destroyFunction={this.destroyFunction} handleToggle={this.handleToggle} />
        <footer className="footer">
          <span className="todo-count">
            <strong>{this.state.todos.length}</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.clearCompleted}>
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}
// handleNewAddTodo(ev){
//   //on Enter then add ev.target.target to state
// }
class TodoItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input id={this.props.id} className="toggle" type="checkbox" onClick={this.props.handleToggle} />
          <label>{this.props.title}</label>
          <button onClick={this.props.destroyFunction} className="destroy" />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              title={todo.title}
              key={todo.id}
              id={todo.id}
              completed={todo.completed}
              destroyFunction={this.props.destroyFunction}
              handleToggle={this.props.handleToggle}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
